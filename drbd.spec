Name:           drbd
Summary:        DRBD user-land tools and scripts
Version:        9.29.0
Release:        9
Source0:        http://www.linbit.com/downloads/%{name}/utils/%{name}-utils-%{version}.tar.gz
Patch0:         drbd-utils-9.12.2-disable_xsltproc_network_read.patch
Patch1:         drbd-utils-9.15.0-make_configure-workaround.patch
Patch2:         backport-drbd-verify.py-relax-host-key-checking.patch
Patch3:         backport-DRBDmon-Disabled-DRBD-commands-warning-only-for-actu.patch
Patch4:         backport-DRBDmon-Integrate-global-local-command-delegation.patch
Patch5:         backport-DRBDmon-Adjust-events-log-supplier-program-name.patch
Patch6:         backport-DRBDmon-Add-drbd-events-log-supplier.patch
Patch7:         backport-DRBDmon-Adjust-Makefile.patch
Patch8:         backport-DRBDmon-Version-V1R4M1.patch
Patch9:         backport-drbdadm-add-proxy-options-to-add-connection-command.patch
Patch10:        backport-Fix-typo-in-warning-there-is-no-po4a-translage-comma.patch
Patch11:        backport-drbd.ocf-explicitly-timeout-crm_master-IPC-early.patch
Patch12:        backport-drbd.ocf-the-text-output-of-crm_resource-locate-has-.patch
Patch13:        backport-WinDRBD-Retry-installing-bus-device-on-timeout.patch
Patch14:        backport-WinDRBD-windrbd-lock-driver-unlock-driver-commands.patch
Patch15:        backport-WinDRBD-Suspend-Resume-I-O-for-a-minor-on-the-WinDRB.patch
Patch16:        backport-windrbd-update-size-command.patch
Patch17:        backport-Update-size-2.patch
Patch18:        backport-Removed-warning-about-sector-size.patch
Patch19:        backport-WinDRBD-Timestamps-in-user-mode-helper-daemon.patch
Patch20:        backport-WinDRBD-dup2-stderr-on-stdout-in-umhelper.patch
Patch21:        backport-WinDRBD-do-not-exit-umhelper-daemon-on-size-mismatch.patch
Patch22:        backport-DRBDmon-exceptions-delete-defaulted-copy-constructor.patch
Patch23:        backport-DRBDmon-Remove-non-functional-environment-variables-.patch

License:        GPL-2.0-or-later
ExclusiveOS:    linux
URL:            http://www.drbd.org/
BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  flex
BuildRequires:  libxslt
BuildRequires:  docbook-style-xsl
BuildRequires:  perl-generators
BuildRequires:  po4a
BuildRequires:  rubygem-asciidoctor
BuildRequires:  keyutils-libs-devel
Requires:       %{name}-utils = %{version}
Requires:       %{name}-udev = %{version}
BuildRequires:  udev
BuildRequires:  make

%description
DRBD refers to block devices designed as a building block to form high
availability (HA) clusters. This is done by mirroring a whole block device
via an assigned network. DRBD can be understood as network based raid-1.

This is a virtual package, installing the full user-land suite.

%files
%doc COPYING
%doc ChangeLog

%prep
%setup -q -n drbd-utils-%{version}

# Don't let xsltproc make network calls during build
%autopatch -p1

%build
%configure \
    --with-utils \
    --without-km \
    --with-udev \
%ifarch %{ix86} x86_64
    --with-xen \
%else
    --without-xen \
%endif
    --with-pacemaker \
    --with-rgmanager \
    --with-distro=generic \
    --with-systemdunitdir=%{_unitdir}
%{make_build}

%install
rm -rf $RPM_BUILD_ROOT
%{make_install}

# Remove old init script, replace with systemd unit file
rm -f $RPM_BUILD_ROOT/%{_initddir}/drbd
install -d -m755 $RPM_BUILD_ROOT/%{_unitdir}

# Remove old heartbeat files that aren't needed any longer in Fedora
rm -rf $RPM_BUILD_ROOT/etc/ha.d

%package utils
Summary: Management utilities for DRBD

%description utils
DRBD mirrors a block device over the network to another machine.
Think of it as networked raid 1. It is a building block for
setting up high availability (HA) clusters.

This packages includes the DRBD administration tools.

%files utils
%defattr(755,root,root,-)
%{_sbindir}/drbdsetup
%{_sbindir}/drbdadm
%{_sbindir}/drbdmeta
%{_sbindir}/drbdmon
%{_sbindir}/drbd-events-log-supplier

# systemd-related stuff
%attr(0644,root,root) %{_unitdir}/drbd.service
%attr(0644,root,root) %{_unitdir}/drbd-graceful-shutdown.service
%attr(0644,root,root) %{_unitdir}/drbd-demote-or-escalate@.service
%attr(0644,root,root) %{_unitdir}/drbd-lvchange@.service
%attr(0644,root,root) %{_unitdir}/drbd-promote@.service
%attr(0644,root,root) %{_unitdir}/drbd-reconfigure-suspend-or-error@.service
%attr(0644,root,root) %{_unitdir}/drbd-services@.target
%attr(0644,root,root) %{_unitdir}/drbd-wait-promotable@.service
%attr(0644,root,root) %{_unitdir}/drbd@.service
%attr(0644,root,root) %{_unitdir}/drbd@.target
%attr(0644,root,root) %{_unitdir}/ocf.ra@.service
%attr(0644,root,root) %{_tmpfilesdir}/%{name}.conf

# Yes, these paths are peculiar. Upstream is peculiar.
# Be forewarned: rpmlint hates this stuff.
%defattr(755,root,root,-)
%{_prefix}/lib/drbd/scripts/drbd
%{_prefix}/lib/drbd/scripts/drbd-service-shim.sh
%{_prefix}/lib/drbd/scripts/drbd-wait-promotable.sh
%{_prefix}/lib/drbd/scripts/ocf.ra.wrapper.sh
%{_prefix}/lib/drbd/drbdadm-*
%{_prefix}/lib/drbd/drbdsetup-*
/usr/lib/drbd/*.sh
/usr/lib/drbd/rhcs_fence

%defattr(-,root,root,-)
%dir %{_var}/lib/%{name}
%config(noreplace) %{_sysconfdir}/drbd.conf
%dir %{_sysconfdir}/drbd.d
%config(noreplace) %{_sysconfdir}/drbd.d/global_common.conf
%config(noreplace) %{_sysconfdir}/multipath/conf.d/drbd.conf
%{_mandir}/man8/drbd*gz
%{_mandir}/man5/drbd*gz
%{_mandir}/ja/man5/drbd*gz
%{_mandir}/ja/man8/drbd*gz
%{_mandir}/man7/drbd*@.service.*
%{_mandir}/man7/drbd*@.target.*
%{_mandir}/man7/drbd.service.*
%{_mandir}/man7/ocf.ra@.service.*
%doc scripts/drbd.conf.example
%license COPYING
%doc ChangeLog


# armv7hl/aarch64 doesn't have Xen packages
%ifarch %{ix86} x86_64
%package xen
Summary: Xen block device management script for DRBD
Requires: %{name}-utils = %{version}-%{release}

%description xen
This package contains a Xen block device helper script for DRBD, capable of
promoting and demoting DRBD resources as necessary.

%files xen
%defattr(755,root,root,-)
%{_sysconfdir}/xen/scripts/block-drbd
%endif


%package udev
Summary: udev integration scripts for DRBD
Requires: %{name}-utils = %{version}-%{release}, udev

%description udev
This package contains udev helper scripts for DRBD, managing symlinks to
DRBD devices in /dev/drbd/by-res and /dev/drbd/by-disk.

%files udev
%{_udevrulesdir}/65-drbd.rules


%package pacemaker
Summary: Pacemaker resource agent for DRBD
Requires: %{name}-utils = %{version}-%{release}
Requires: pacemaker
License: GPL-2.0-only

%description pacemaker
This package contains the master/slave DRBD resource agent for the
Pacemaker High Availability cluster manager.

%files pacemaker
%defattr(755,root,root,-)
%dir %{_prefix}/lib/ocf/resource.d/linbit/
%{_prefix}/lib/ocf/resource.d/linbit/drbd
%{_prefix}/lib/ocf/resource.d/linbit/drbd-attr
%{_prefix}/lib/ocf/resource.d/linbit/drbd.shellfuncs.sh
%{_mandir}/man7/ocf_linbit_drbd*gz


%package rgmanager
Summary: Red Hat Cluster Suite agent for DRBD
Requires: %{name}-utils = %{version}-%{release}

%description rgmanager
This package contains the DRBD resource agent for the Red Hat Cluster Suite
resource manager.

As of Red Hat Cluster Suite 3.0.1, the DRBD resource agent is included
in the Cluster distribution.

%files rgmanager
%defattr(755,root,root,-)
%{_datadir}/cluster/drbd.sh

%defattr(-,root,root,-)
%{_datadir}/cluster/drbd.metadata


%package bash-completion
Summary: Programmable bash completion support for drbdadm
Requires: %{name}-utils = %{version}-%{release}

%description bash-completion
This package contains programmable bash completion support for the drbdadm
management utility.

%files bash-completion
%config %{_sysconfdir}/bash_completion.d/drbdadm*


%post utils
%systemd_post drbd.service

%preun utils
%systemd_preun drbd.service

%changelog
* Thu Feb 20 2025 liupei <liupei@kylinos.cn> - 9.29.0-9
- WinDRBD: do not exit umhelper daemon on size mismatch.
- DRBDmon: exceptions: delete defaulted copy constructor & operator
- DRBDmon: Remove non-functional environment variables, update CLI help text

* Mon Feb 17 2025 liupei <liupei@kylinos.cn> - 9.29.0-8
- Removed warning about sector size
- WinDRBD: Timestamps in user mode helper daemon
- WinDRBD: dup2() stderr on stdout in umhelper

* Fri Feb 14 2025 liupei <liupei@kylinos.cn> - 9.29.0-7
- WinDRBD: Suspend/Resume I/O for a minor on the WinDRBD level (outside DRBD)
- windrbd update-size command
- Update size 2

* Wed Feb 12 2025 liupei <liupei@kylinos.cn> - 9.29.0-6
- WinDRBD: Retry installing bus device on timeout
- WinDRBD: windrbd lock-driver / unlock-driver commands

* Thu Dec 19 2024 liupei <liupei@kylinos.cn> - 9.29.0-5
- Fix typo in warning, there is no po4a-translage command
- drbd.ocf: explicitly timeout crm_master IPC early
- drbd.ocf: the text output of "crm_resource --locate" has changed

* Mon Dec 09 2024 liupei <liupei@kylinos.cn> - 9.29.0-4
- DRBDmon: Add drbd-events-log-supplier
- drbdadm: add proxy options to 'add connection' command

* Fri Dec 06 2024 liupei <liupei@kylinos.cn> - 9.29.0-3
- DRBDmon: Integrate global/local command delegation
- DRBDmon: Adjust events log supplier program name

* Tue Dec 03 2024 liupei <liupei@kylinos.cn> - 9.29.0-2
- drbd-verify.py: relax host key checking
- DRBDmon: Disabled DRBD commands warning only for actual DRBD commands

* Tue Nov 26 2024 liupei <liupei@kylinos.cn> - 9.29.0-1
- update to 9.29.0
- drbdmon: various improvements
- events2: expose if device is open

* Sun Nov 24 2024 liupei <liupei@kylinos.cn> - 9.28.0-17
- DRBDmon: Disable actions option & hotkey if DRBD actions are disabled
- DRBDmon: Enable right-click on the peer volumes list
- DRBDmon: Version V1R3M0

* Fri Nov 22 2024 liupei <liupei@kylinos.cn> - 9.28.0-16
- DRBDmon: Disable DRBD actions/commands when showing an events log file
- DRBDmon: Modify restart behavior when displaying an events log file

* Wed Nov 20 2024 liupei <liupei@kylinos.cn> - 9.28.0-15
- DRBDmon: Pass-through the monitor environment

* Thu Oct 31 2024 liupei <liupei@kylinos.cn> - 9.28.0-14
- DRBDmon: Fix CfgEntryStore UUID
- DRBDmon: Update display interval field on configuration load or reset
- DRBDmon: Declare AnsiControl destructor virtual
- DRBDmon: Busy indicator, debug log updates

* Tue Oct 29 2024 liupei <liupei@kylinos.cn> - 9.28.0-13
- DRBDmon: Add help text for new DRBD and DRBDmon commands

* Fri Oct 25 2024 liupei <liupei@kylinos.cn> - 9.28.0-12
- DRBDmon: MDspTaskDetail: Reinitialize information if the selected task ID changes
- DRBDmon: Version V1R2M6

* Fri Oct 18 2024 liupei <liupei@kylinos.cn> - 9.28.0-11
- DRBDmon: Add /CLOSE command
- DRBDmon: Update command description for invalidate, improved clarity
- DRBDmon: Add debug log displays

* Mon Oct 14 2024 liupei <liupei@kylinos.cn> - 9.28.0-10
- DRBDmon: Fix resource & volume selection count labels 
- DRBDmon: Always prefix completed DRBD commands with two slashes
- DRBDmon: Add DRBD commands, logic for peer volume commands

* Thu Oct 10 2024 liupei <liupei@kylinos.cn> - 9.28.0-9
- DRBDmon: Remove replication state from volume details, it's always unknown
- DRBDmon: Adjust display_activated method

* Mon Sep 23 2024 liupei <liupei@kylinos.cn> - 9.28.0-8
- DRBDmon: Adjust display IDs for peer volume list, details, actions
- DRBDmon: Add MDspPeerVolumeDetail and MDspPeerVolumeActions

* Wed Sep 18 2024 liupei <liupei@kylinos.cn> - 9.28.0-7
- DRBDmon: Add help text skeletons for peer volume details & actions displays
- DRBDmon: Add constant ARG_INVALIDATE_REMOTE

* Thu Sep 12 2024 liupei <liupei@kylinos.cn> - 9.28.0-6
- DRBDmon: Move peer volume selection to SharedData
- DRBDmon: Fix volume details minor number label
- DRBDmon: Update shared volume cursor using the update

* Sun Sep 08 2024 liupei <liupei@kylinos.cn> - 9.28.0-5
- DRBDmon: Add commands /select, /deselect, /deselect-all (alias for /clear-selection)
- DRBDmon: Make change_selection private

* Tue Sep 03 2024 liupei <liupei@kylinos.cn> - 9.28.0-4
- DRBDmon: Change /cursor command, add /resource, /connection, /volume commands
- DRBDmon: Version V1R2M5

* Fri Aug 30 2024 liupei <liupei@kylinos.cn> - 9.28.0-3
- tests: export sanitize environment
- DRBDmon: Add StringTokenizer methods restart & advance
- DRBDmon: Add string_matching module

* Wed Aug 28 2024 liupei <liupei@kylinos.cn> - 9.28.0-2
- drbd: fix initialization of "bitmap area" for external meta data
- drbd_buildtag.h: only update if changed

* Tue Aug 13 2024 liupei <liupei@kylinos.cn> - 9.28.0-1
- update to 9.28.0
- events2: set may_promote:no promotion_score:0 while force-io-failure:yes
- drbdsetup,v9: show TLS in connection status
- drbdsetup,v9: add udev command
- crm-fence-peer.9.sh: fixes for pacemaker 2.1.7
- events2: improved out of order message handling

* Tue Aug 13 2024 liupei <liupei@kylinos.cn> - 9.27.0-12
- drbdsetup,events2: remove a pointless loop
- drbdsetup,events2: make apply_stored_event() a function
- drbdsetup,events2: Reorder events that arrive out-of-order

* Thu Aug 08 2024 liupei <liupei@kylinos.cn> - 9.27.0-11
- tests: add tests for out-of-order events2 messages

* Tue Aug 06 2024 liupei <liupei@kylinos.cn> - 9.27.0-10
- drbdmeta: when asking pvs for pv_size, allow scan_lvs=1
- drbd.rules: remove GOTO without corresponding LABEL

* Wed Jul 24 2024 liupei <liupei@kylinos.cn> - 9.27.0-9
- crm-fence-peer.9.sh: fix parsing in_ccm crmd fields of node_state with Pacemaker 2.1.7
- crm-fence-peer.9.sh: use join of node_state to judge whether node is banned

* Mon Jul 15 2024 liupei <liupei@kylinos.cn> - 9.27.0-8
- configure.ac: Add an option to disable host udev version checks
- drbdsetup: add udev command
- drbd.rules: use drbdsetup udev command

* Wed Jul 10 2024 liupei <liupei@kylinos.cn> - 9.27.0-7
- drbdsetup,v9: show TLS in connection status
- Filter DRBD devices from LVM commands

* Fri Jun 28 2024 liupei <liupei@kylinos.cn> - 9.27.0-6
- add missing include for recently added call to include_file() 
- drbdmeta: create-md: new options --effective-size and --diskful-peers
- drbdadm: create-md: new options --effective-size, --diskful-peers 

* Sat Mar 23 2024 liupei <liupei@kylinos.cn> - 9.27.0-5
- fix typo in error message 

* Thu Mar 21 2024 liupei <liupei@kylinos.cn> - 9.27.0-4
- events2: set may_promote:no promotion_score:0 while force-io-failure:yes 

* Tue Mar 19 2024 liupei <liupei@kylinos.cn> - 9.27.0-3
- Update scripts/drbd to fix "no such file" bug

* Mon Mar 18 2024 liupei <liupei@kylinos.cn> - 9.27.0-2
- postparse: improve "file:line: error message" reporting 

* Fri Mar 15 2024 liupei <liupei@kylinos.cn> - 9.27.0-1
- update to 9.27.0

* Mon Mar 11 2024 liupei <liupei@kylinos.cn> - 9.26.0-1
- update to 9.26.0
- Build requires keyutils-libs-devel

* Fri Mar 8 2024 liupei <liupei@kylinos.cn> - 9.25.0-1
- update to 9.25.0

* Tue Mar 5 2024 liupei <liupei@kylinos.cn> - 9.24.0-1
- update to 9.24.0

* Thu Feb 29 2024 liupei <liupei@kylinos.cn> - 9.23.1-1
- update to 9.23.1

* Wed Feb 28 2024 liupei <liupei@kylinos.cn> - 9.23.0-1
- update to 9.23.0

* Wed Feb 28 2024 liupei <liupei@kylinos.cn> - 9.22.0-1
- update to 9.22.0

* Mon Nov 27 2023 liupei <liupei@kylinos.cn> - 9.21.4-1
- update to 9.21.4

* Fri Mar 11 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 9.17.0-1
- update to 9.17.0

* Fri Aug 13 2021 yangzhao <yangzhao1@kylinos.cn> - 9.5.0-6
- Fix gcc 10 build multiple definition error

* Thu Nov 27 2020 jiangxinyu <jiangxinyu@kylinos.cn> - 9.5.0-5
- Resolve resource-agents conflict when installing drbd-rgmanager package

* Fri Oct 30 2020 jiangxinyu <jiangxinyu@kylinos.cn> - 9.5.0-4
- Rebuilt for openEuler20.03 LTS

* Thu Apr 16 2020 houjian<jian.hou@kylinos.cn> - 9.5.0-3
- Init drbd  project
